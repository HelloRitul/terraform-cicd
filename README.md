# DevOps Project - Automate Terraform with GitLab CI/CD Pipeline

## Overview

This project aims to automate infrastructure provisioning using Terraform and implement continuous integration and deployment (CI/CD) pipelines with GitLab. It includes setting up Terraform, AWS CLI, writing Terraform configurations, creating a GitLab repository, configuring CI/CD pipeline, and managing AWS resources.

## Prerequisites

Ensure the following software is installed on your local system:

- [ ] Terraform
- [ ] AWS CLI
- [ ] Git
- [ ] Visual Studio Code (or any preferred text editor)


## Project Setup

1. Terraform Configuration:

- [ ] Write Terraform configuration files (*.tf) defining the desired infrastructure resources.

- [ ] Create a .gitignore file to exclude sensitive or unnecessary files from being tracked by Git.

2. GitLab Repository:

- [ ] Create a new repository on GitLab to host the Terraform code and CI/CD configurations.

- [ ] Upload all Terraform code to the GitLab repository.

3. Git Initialization:

- [ ] Initialize a Git repository in your local Terraform project directory.

```
git init

```

4. AWS Account Setup:

- [ ] Set up an AWS account if you haven't already.

- [ ] Configure AWS CLI with your AWS access key and secret key.

## GitLab CI/CD Pipeline

1. GitLab Configuration:

- [ ] Create a .gitlab-ci.yml file in the root of your repository to define the CI/CD pipeline.

2. Pipeline Stages:

- [ ] Define stages for validation, planning, applying, and destroying Terraform resources.


- [ ] Use manual triggers for apply and destroy stages to prevent accidental deployments.


3. Variables Configuration:

- [ ] Add AWS access key and secret key as environment variables in GitLab project settings.

## CI/CD Pipeline Execution

1. Local Development:

- [ ] Write Terraform configurations in Visual Studio Code or any preferred text editor.

2. Git Operations:

- [ ] Add and commit your Terraform files to the Git repository.

```
git add .

git commit -m "Initial Terraform configuration"
```

Create a new branch (dev) for development and push the changes.

```
git checkout -b dev

git push origin dev
```


Merge dev branch into main branch once development is complete.

```
git checkout main

git merge dev
```

3. Pipeline Execution:

- [ ] GitLab CI/CD pipeline will automatically trigger on every commit to the main branch.

- [ ] Review pipeline execution in GitLab UI.


4. Manual Deployment:

- [ ] Manually trigger apply stage to apply Terraform changes.


- [ ] Manually trigger destroy stage to destroy Terraform resources.


5. Verification:

- [ ] Check AWS Management Console to ensure resources are created/destroyed as expected.


# Conclusion

By following these steps, you have successfully set up a CI/CD pipeline to automate Terraform deployments using GitLab. All Terraform code is uploaded to the GitLab repository, ensuring version control and collaboration among team members. The pipeline ensures consistent and reliable infrastructure provisioning while minimizing manual interventions.

This update reflects the project's focus on automating Terraform deployments with a GitLab CI/CD pipeline.






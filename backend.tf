terraform {
  backend "s3" {
    bucket = "terraformproject1bucket"
    key    = "terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "dynamo-terraform-table"
  }
}
